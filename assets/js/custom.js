$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        stagePadding: 50,
        loop:true,
        margin:-100,
        nav:true,
        items:1,
        lazyLoad: true,
        nav:true,
        dots:true,
        dotsEach: 1,
        navText: [
          "<i class='fa fa-caret-left'></i>",
          "<i class='fa fa-caret-right'></i>"
        ],
        center:true,
        responsive:{
            320: {
              items: 1,
              stagePadding: 0
            },
            600: {
              items: 3
            },
            997: {
              items: 3
            }
        }
    });

    // set cookie according to you
    var cookieName= "CodingStatus";
    var cookieValue="Coding Tutorials";
    var cookieExpireDays= 30;
    // when users click accept button
    let acceptCookie= document.getElementById("acceptCookie");
    acceptCookie.onclick= function(){
        createCookie(cookieName, cookieValue, cookieExpireDays);
    }
    // function to set cookie in web browser
     let createCookie= function(cookieName, cookieValue, cookieExpireDays){
      let currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + (cookieExpireDays*24*60*60*1000));
      let expires = "expires=" + currentDate.toGMTString();
      document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
      if(document.cookie){
        document.getElementById("cookiePopup").style.display = "none";
      }else{
        alert("Unable to set cookie. Please allow all cookies site from cookie setting of your browser");
      }
     }
    // get cookie from the web browser
    let getCookie= function(cookieName){
      let name = cookieName + "=";
      let decodedCookie = decodeURIComponent(document.cookie);
      let ca = decodedCookie.split(';');
      for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }
    // check cookie is set or not
    let checkCookie= function(){
        let check=getCookie(cookieName);
        if(check==""){
            document.getElementById("cookiePopup").style.display = "block";
        }else{
            
            document.getElementById("cookiePopup").style.display = "none";
        }
    }
    checkCookie();

    $('.menu-wrapper .parent-nav').click(function (e) {

        
        if($(window).width() < 992) {
          e.preventDefault();
          if ($(this).has('.sub-menu').length) {
            
            var item = $( this ).find( ".sub-menu" );

            if (item.length) {

              $(item).toggle(function () {
                  item.css({ visibility: "visible"});
              }, function () {
         
              });
            }
         
          }

        }


    });

    $(".menu-toggle, .menu-wrapper li").click(function(e) {
      e.stopPropagation(); //stops click event from reaching document
    });



    $(document).click(function(){
        
        if($(window).width() < 992) {
          $('.menu-wrapper nav').removeClass('active-nav');
          $('.sub-menu').css({ display: "none"});
        }
    });

    window.onload = function() {
        var $recaptcha = document.querySelector('#g-recaptcha-response');

        if($recaptcha) {
            $recaptcha.setAttribute("required", "required");
        }
    };

});